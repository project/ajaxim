function trim(text) {
   if(text == null) return null;
   return text.replace(/^[ \t]+|[ \t]+$/g, "");
}

function toggleStatusList() {
   if($ID('statusList').style.display == 'block') {
      $ID('statusList').style.display = 'none';
      //if($ID('statusList').style.zIndex > Windows.maxZIndex) Windows.maxZIndex = $ID('statusList').style.zIndex;
   } else {
      //$ID('statusList').style.left = $ID('statusSettings').offsetLeft + $ID('blTopToolbar').offsetLeft + 'px';
      $ID('statusList').style.top = $ID('statusSettings').offsetTop + $ID('blTopToolbar').offsetTop + $ID('statusSettings').offsetHeight + 'px';
      $ID('statusList').style.zIndex = 220;
      $ID('statusList').style.display = 'block';
   }
}

function setStatus(status, away_msg) {
   if(status == 1) { // away
      isAway = 1;
      awayMessage = away_msg;
      $ID('curStatus').innerHTML = awayMessage.substring(0, 30) + (awayMessage.length > 30 ? '...' : '');
   } else { // back
      isAway = status; // 0 for avail, 99 for "friends only", 49 for "invisible"
      awayMessage = '';
      $ID('curStatus').innerHTML = away_msg;
   }
   curStatus = $ID('curStatus').innerHTML;
   $ID('statusList').style.display = 'none';
}

function customAway() {
   $ID('curStatus').style.display = 'none';
   $ID('customStatus').style.display = 'block';
   $ID('customStatus').focus();
}

function processCustomAway(e) {
   var asc = document.all ? event.keyCode : e.which;

   if(asc == 13) {
      isAway = 1;
      awayMessage = $ID('customStatus').value;
      $ID('curStatus').innerHTML = awayMessage.substring(0, 30) + (awayMessage.length > 30 ? '...' : '');
      $ID('curStatus').style.display = 'block';
      $ID('customStatus').style.display = 'none';
   }
   return asc != 13;
}

function showHide(evt) {
     if (!evt) { evt = window.event; }
     if (document.all) { trgObj = evt.srcElement; }
     else { trgObj = evt.target; }
     if (!trgObj) { return; }
     if (trgObj.id != 'statusList' && trgObj.id != 'fontsList' && trgObj.id != 'statusSettings'
          && trgObj.id != 'curStatus' && trgObj.parentNode.id != 'statusList' &&
          trgObj.parentNode.id != 'fontsList' && trgObj.id != 'customMessage' &&
          trgObj.parentNode.id != 'customMessage' && trgObj.id != 'emoticonList' &&
          trgObj.className != 'emotIcon' && trgObj.id != 'fontSizeList' &&
          trgObj.parentNode.id != 'fontSizeList' && trgObj.id != 'fontColorList' &&
          trgObj.className != 'colorItem' &&
          trgObj.className != 'tTable') {
        document.getElementById('statusList').style.display = 'none';
        document.getElementById('fontsList').style.display = 'none';
        document.getElementById('emoticonList').style.display = 'none';
        document.getElementById('fontSizeList').style.display = 'none';
        document.getElementById('fontColorList').style.display = 'none';
        return;
     }
}

function blinkerOn(onoff) {
   if(onoff == true)
      titlebarBlinker = true;
   else
      titlebarBlinker = false;
}

function handleInput(e, func) {
   var asc = document.all ? event.keyCode : e.which;

   if(asc == 13) {
      func();
      return false;
   }

   return true;
}

function handleResize(eventName, win) {
   if(win.getId() == 'bl') {
      sizeBuddyList();
   } else if(win.getId().indexOf('_im') != -1) {
      var mastername = win.getId().replace(/_im/, '');
      $ID(mastername + '_rcvd').style.height = (win.getSize()['height'] - 103) + 'px';
      $ID(mastername + '_rcvd').style.width = (win.getSize()['width'] - 10) + 'px';

      $ID(mastername + '_toolbar').style.top = (win.getSize()['height'] - 73) + 'px';
      $ID(mastername + '_toolbar').style.width = (win.getSize()['width'] - 10) + 'px';

      $ID(mastername + '_setFont').style.top = (win.getSize()['height'] - 65) + 'px';

      $ID(mastername + '_setFontSize').style.top = (win.getSize()['height'] - 65) + 'px';

      $ID(mastername + '_setFontColor').style.top = (win.getSize()['height'] - 65) + 'px';

      $ID(mastername + '_insertEmoticon').style.top = (win.getSize()['height'] - 65) + 'px';

      $ID(mastername + '_sendBox').style.top = (win.getSize()['height'] - 45) + 'px';
      $ID(mastername + '_sendBox').style.width = (win.getSize()['width'] - 16) + 'px';

      var curIM = $ID(win.getId().replace(/_im/, '_rcvd'));
      curIM.scrollTop = curIM.scrollHeight - curIM.clientHeight + 6;
   }
}

function handleClose(eventName, win) {
   if(win.getId().indexOf('_im') == -1) return;

   //leaveRoom(win.getId().replace(/_im/, ''));

   var rcvdBox = $ID(win.getId().replace(/_im/, '') + '_rcvd');
   if(imHistory == true) {
      rcvdBox.innerHTML = '<span class="imHistory">' +
                          rcvdBox.innerHTML.replace(/\(Auto-Reply:\)/g, 'Auto-Reply:').replace(/<(?![Bb][Rr] ?\/?)([^>]+)>/ig, '') +
                          "</span>\n";
   } else {
      rcvdBox.innerHTML = '';
   }
}

function handleMinimize(eventName, win) {
   if(win.getId().indexOf('_im') == -1) return;

   var curIM = $ID(win.getId().replace(/_im/, '_rcvd'));
   curIM.scrollTop = curIM.scrollHeight - curIM.clientHeight + 6;
}

function toggleFontList(win) {
   $ID('emoticonList').style.display = 'none';
   $ID('fontSizeList').style.display = 'none';
   $ID('fontColorList').style.display = 'none';
   if($ID('fontsList').style.display == 'block') {
      $ID('fontsList').style.display = 'none';
      toWin = '';
   } else {
      $ID('fontsList').style.left = (parseInt(IMWindows[win].getLocation()['left']) + parseInt($ID(win + '_setFont').offsetLeft)) + 'px';
      $ID('fontsList').style.top = (parseInt(IMWindows[win].getLocation()['top']) + parseInt(IMWindows[win].getSize()['height']) - 46) + 'px';
      $ID('fontsList').style.zIndex = Windows.maxZIndex + 20;
      $ID('fontsList').style.display = 'block';
      toWin = win;
   }
}

function toggleFontSizeList(win) {
   $ID('emoticonList').style.display = 'none';
   $ID('fontsList').style.display = 'none';
   $ID('fontColorList').style.display = 'none';
   if($ID('fontSizeList').style.display == 'block') {
      $ID('fontSizeList').style.display = 'none';
      toWin = '';
   } else {
      $ID('fontSizeList').style.left = (parseInt(IMWindows[win].getLocation()['left']) + parseInt($ID(win + '_setFontSize').offsetLeft)) + 'px';
      $ID('fontSizeList').style.top = (parseInt(IMWindows[win].getLocation()['top']) + parseInt(IMWindows[win].getSize()['height']) - 46) + 'px';
      $ID('fontSizeList').style.zIndex = Windows.maxZIndex + 20;
      $ID('fontSizeList').style.display = 'block';
      toWin = win;
   }
}

function toggleEmoticonList(win) {
   $ID('fontsList').style.display = 'none';
   $ID('fontSizeList').style.display = 'none';
   $ID('fontColorList').style.display = 'none';
   if($ID('emoticonList').style.display == 'block') {
      $ID('emoticonList').style.display = 'none';
      toWin = '';
   } else {
      $ID('emoticonList').style.left = (parseInt(IMWindows[win].getLocation()['left']) + parseInt($ID(win + '_insertEmoticon').offsetLeft)) + 'px';
      $ID('emoticonList').style.top = (parseInt(IMWindows[win].getLocation()['top']) + parseInt(IMWindows[win].getSize()['height']) - 46) + 'px';
      $ID('emoticonList').style.zIndex = Windows.maxZIndex + 20;
      $ID('emoticonList').style.display = 'block';
      toWin = win;
   }
}

function toggleFontColorList(win) {
   $ID('fontsList').style.display = 'none';
   $ID('fontSizeList').style.display = 'none';
   $ID('emoticonList').style.display = 'none';
   if($ID('fontColorList').style.display == 'block') {
      $ID('fontColorList').style.display = 'none';
      toWin = '';
   } else {
      $ID('fontColorList').style.left = (parseInt(IMWindows[win].getLocation()['left']) + parseInt($ID(win + '_setFontColor').offsetLeft)) + 'px';
      $ID('fontColorList').style.top = (parseInt(IMWindows[win].getLocation()['top']) + parseInt(IMWindows[win].getSize()['height']) - 46) + 'px';
      $ID('fontColorList').style.zIndex = Windows.maxZIndex + 20;
      $ID('fontColorList').style.display = 'block';
      toWin = win;
   }
}

function titlebarBlink(name, message, alter) {
   if(titlebarBlinker == false) {
      document.title = defaultTitle;
      return;
   }

   if(IMWindows[name].popup) {
      IMWindows[name].popup.titlebarBlink(name, message, alter);
      return;
   }

   if(alter == 0) {
      document.title = name + '!';
      blinkerTimer = setTimeout("titlebarBlink('"+name+"', '"+message+"', 1)", 1000);
   } else if(alter == 1) {
      document.title = '"' + message.substring(0, 10) + (message.length > 10 ? '...' : '') + '"';
      blinkerTimer = setTimeout("titlebarBlink('"+name+"', '"+message+"', 2)", 1000);
   } else if(alter == 2) {
      document.title = defaultTitle;
      blinkerTimer = setTimeout("titlebarBlink('"+name+"', '"+message+"', 0)", 1000);
   }
}
