// Notification //
var useBlinker    = true;           // Show new message in titlebar when window isn't active.
var blinkSpeed    = 1000;           // How fast to change between the titles when "blinking" (in milliseconds).
var pulsateTitles = true;           // Pulsate (blink) IM window titles on new IM when they are not the active window.
var audioNotify   = true;           // By default, play sounds upon getting an IM?
var blinkerTimer;

var pingFrequency = 5000;
var pingTimer;
var buddyList = {};
var windowCSS     = 'default';
var curSelected='';
var IMWindows={};
var user='';

var isAway=0;
var awayMessage = '';
var titlebarBlinker=false;
var toWin;
var newWin, newWinRcvd;


// Windows //
var imWidth       = 310;            // Default IM window width
var imHeight      = 335;            // Default IM window height
var imHistory     = true;           // "Save" conversations with buddies throughout the session
var imDetachable  = true;           // Enable/Disable ability to detach windows from the application

var smilies = [                    // Smiley format: 'smiley~imagename.ext',
   ":)~smile.gif",
   ":-)~smile.gif",
   ":D~grin.gif",
   ":-D~grin.gif",
   ";)~wink.gif",
   ";-)~wink.gif",
   ":[~embarassed.gif",
   ":-[~embarassed.gif",
   ":*~love.gif",
   ":-*~love.gif",
   ":(~sad.gif",
   ":-(~sad.gif",
   ":'(~cry.gif",
   ":'-(~cry.gif",
   "8-)~cool.gif",
   "&gt;:o~angry.gif",
   "&gt;:-o~angry.gif",
   ":O~surprised.gif",
   ":-O~surprised.gif",
   ":|~serious.gif",
   ":-|~serious.gif",
   ":-S~sick.gif",
   ":P~tongue.gif",
   ":-P~tongue.gif",
   "&lt;3~heart.gif",
   "&lt;33~heart.gif",
   "&lt;\/3~heartbroken.gif",
   "&lt;\\3~heartbroken.gif"
];

soundManager.url = base_path + 'soundmanager2.swf';
soundManager.debugMode = false;
//soundManager.consoleOnly = false;
soundManager.onload = function() {
  soundManager.createSound('msg_out', base_path+'sounds/msg_out.mp3');
  soundManager.createSound('msg_in', base_path+'sounds/msg_in.mp3');
}

$(document).ready(function() {
  Windows.addObserver({ onResize: handleResize });
  Windows.addObserver({ onClose: handleClose });
  Windows.addObserver({ onMaximize: handleResize });
  Windows.addObserver({ onMinimize: handleMinimize });

  if(isDefined(window.onfocus) && isDefined(window.onblur) && isDefined(blinkerOn)) {
    window.onfocus = function() { blinkerOn(false); };
    window.onblur = function() { blinkerOn(true); };
  } else if(isDefined(blinkerOn)){
    document.onfocus = function() { blinkerOn(false); };
    document.onblur = function() { blinkerOn(true); };
  }

  var windowScroll = WindowUtilities.getWindowScroll();
  var pageSize = WindowUtilities.getPageSize();

  var top = (pageSize.windowHeight - 529)/2;
  top += windowScroll.top;

  var left = (pageSize.windowWidth - 762)/2;
  left += windowScroll.left;
});

function dump(arr,level) {
var dumped_text = "";
if(!level) level = 0;

//The padding given at the beginning of the line.
var level_padding = "";
for(var j=0;j<level+1;j++) level_padding += "    ";

if(typeof(arr) == 'object') { //Array/Hashes/Objects
 for(var item in arr) {
  var value = arr[item];

  if(typeof(value) == 'object') { //If it is an array,
   dumped_text += level_padding + "'" + item + "' ...\n";
   dumped_text += dump(value,level+1);
  } else {
   dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
  }
 }
} else { //Stings/Chars/Numbers etc.
 dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
}
return dumped_text;
}

function new_im_dialog() {
  var newIMWin;
    if($ID('newIM')) {
      Windows.getWindow('newIM').toFront();
      return;
  }

  newIMWin = new Window('newIM',
                        {className: "dialog", width: 240, height: 120, resizable: false, zIndex: 1,
                          title: "New Instant Message...", draggable: true, closable: true, maximizable: false, minimizable: false, detachable: false,
                          minWidth: 240, minHeight: 120, showEffectOptions: {duration: 0}, hideEffectOptions: {duration: 0}});
  newIMWin.getContent().innerHTML = '<div class="dialog_info" style="padding:3px;">Please enter the username of the person you would like to IM.</div> \
                                      <span id="newim_error_msg" style="display:block;width:100%;padding-top:5px;padding-bottom:10px;color:#ff0000;font-weight:bold;text-align:center;">&nbsp;</span> \
                                      <div id="newim_box" style="padding-left:30px;width:100%;"> \
                                      <div style="display:block;float:left;margin-right:5px;padding-top:4px;">Username:</div><input type="text" style="width:120px;" id="sendto" name="sendto" onkeypress="handleInput(event, function() { newIMWindow(); })" /> \
                                      </div> \
                                      <div id="newim_buttons"> \
                                      <img src="'+base_path+'images/' + windowCSS + '/openim.png" onclick="newIMWindow();"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                      <img src="'+base_path+'images/' + windowCSS + '/cancel.png" onclick="Windows.close(\'newIM\');"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                      </div>';
  $ID('newim_buttons').style.position = 'absolute';
  $ID('newim_buttons').style.top      = '110px';
  $ID('newim_buttons').style.left     = '25px';
  newIMWin.setDestroyOnClose();
  newIMWin.showCenter();
  setTimeout("$ID('sendto').focus();", 125);
}

function new_room_dialog() {
  var newRoomWin;
  if($ID('newRoom')) {
      Windows.getWindow('newRoom').toFront();
      return;
  }

  newRoomWin = new Window('newRoom',
                          {className: "dialog", width: 240, height: 120, resizable: false, zIndex: 1,
                            title: "Join a Chatroom...", draggable: true, closable: true, maximizable: false, minimizable: false, detachable: false,
                            minWidth: 240, minHeight: 120, showEffectOptions: {duration: 0}, hideEffectOptions: {duration: 0}});
  newRoomWin.getContent().innerHTML = '<div class="dialog_info" style="padding:3px;">Please enter the name of the room you would like to join.</div> \
                                        <span id="newroom_error_msg" style="display:block;width:100%;padding-top:5px;padding-bottom:10px;color:#ff0000;font-weight:bold;text-align:center;">&nbsp;</span> \
                                        <div id="newroom_box" style="padding-left:25px;width:100%;"> \
                                        <div style="display:block;float:left;margin-right:5px;padding-top:4px;">Room Name:</div><input type="text" style="width:120px;" id="roomname" name="roomname" onkeypress="handleInput(event, function() { joinRoom($ID(\'roomname\').value); })" /> \
                                        </div> \
                                        <div id="newroom_buttons"> \
                                        <img src="'+base_path+'images/' + windowCSS + '/join.png" onclick="joinRoom($ID(\'roomname\').value);"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                        <img src="'+base_path+'images/' + windowCSS + '/cancel.png" onclick="Windows.close(\'newRoom\');"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                        </div>';
  $ID('newroom_buttons').style.position = 'absolute';
  $ID('newroom_buttons').style.top      = '110px';
  $ID('newroom_buttons').style.left     = '25px';
  newRoomWin.setDestroyOnClose();
  newRoomWin.showCenter();
  setTimeout("$ID('roomname').focus();", 125);
}

function login() {
  var xhConn = new XHConn();
  xhConn.connect(pingTo, "POST", "call=login&from="+username,
    function(xh) {
      if(xh.responseText == 'invalid') {
      }
      else {
        user = username;
        var buddydiv = $ID("buddylist");
        buddydiv.innerHTML = '<div id="blTopToolbar"><span class="toolbarButton"><img id="joinroom" src="' +base_path+'images/'+windowCSS+'/joinroom.png" class="toolbarButton" onclick="new_room_dialog();" alt="Join Chatroom" title="Join Chatroom" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);"/></span><span class="toolbarButton"><img id="imanyone" src="' +base_path+'images/'+windowCSS+'/imanyone.png" class="toolbarButton" onclick="new_im_dialog();" alt="IM Anyone" title="IM Anyone" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /></span><span class="toolbarButton"><img id="toggleaudio" src="' +base_path+'images/'+windowCSS+'/audio_'+(audioNotify ? 'on' : 'off')+'.png" onclick="toggleAudio();" alt="Toggle Audio" title="Toggle Audio" /></span><div id="statusSettings"><input type="text" id="customStatus" onkeypress="processCustomAway(event);" style="display:none" onblur="if($ID(\'customStatus\').style.display != \'none\') { $ID(\'customStatus\').style.display = \'none\'; $ID(\'curStatus\').style.display = \'block\'; }" /><a href="#" id="curStatus" onclick="toggleStatusList();return false;">I\'m available</a></div></div><div id="blContainer"><ul id="buddylist" class="sortable box"><li style="display:none"></li></ul></div><!--div id="blBottomToolbar"><a href="#" style="-moz-outline-style: none;" onclick="logout();return false;"><img src="'+base_path+'images/'+windowCSS+'/signoff.png" style="border:0;" alt="Sign off" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /></a></div-->';
        setStatus(fstatus-1, fcurstatus);
        //if(trim(xh.responseText).length == 0) logout();
        var response = xh.responseText.parseJSON();
        pingTimer = setInterval(ping, pingFrequency);
        var buddy;
        if(!response.buddy || response.buddy.length == 0 || response.buddy.length == 2) return;
        var budList = response.buddy.parseJSON();
        for(var group in budList) {
          if(!$ID(group.replace(/\s/, '_')+'_group') && group != 'toJSONString') {
            addGroupToList(group);
          }
          if(!buddyList[group]) buddyList[group] = [];
          for(i=0; i<budList[group].length; i++) {
            buddy = budList[group][i];
            buddyList[group][i] = buddy.username;
            if(!$ID(buddy.username+'_blItem')) addBuddyToList(buddy.username, group);
            $ID(buddy.username+'_blItem').style.display='block';
            if(buddy.is_online == 0 || buddy.is_online == 50) {
              moveBuddy(buddy.username, 'Offline');
              $ID(buddy.username+'_blImg').src = base_path+'images/offline.png';
            } else if(buddy.is_online == 2) {
              moveBuddy(buddy.username, group);
              $ID(buddy.username+'_blImg').src = base_path+'images/away.png';
            } else {
              moveBuddy(buddy.username, group);
              $ID(buddy.username+'_blImg').src = base_path+'images/online.png';
            }
          }
        }
      }
    }
  );
}

function joinRoom(room) {
 var xhConn = new XHConn();
   xhConn.connect(pingTo, "POST", "call=joinroom&from="+user+"&room="+room,
      function(xh) {
         if(xh.responseText.indexOf('"') == -1) {
            switch(xh.responseText) {
               case 'already_joined':
                  $ID('newroom_error_msg').innerHTML = 'You are already in "' + room + '".';
                  break;
               case 'room_is_user':
                  $ID('newroom_error_msg').innerHTML = 'The chosen room name is invalid.';
                  break;
               case 'invalid_chars':
                  $ID('newroom_error_msg').innerHTML = 'Room name contains invalid characters!';
                  break;
            }
         } else {
            if(!$ID(room + '_im')) {
               createIMWindow(room.toLowerCase(), room.toLowerCase());
            } else {
               if(!IMWindows[room].isVisible()) {
                  IMWindows[room].show();
                  setTimeout("scrollToBottom('" + room + "_rcvd')", 125);
               }
            }
            var users = xh.responseText.parseJSON().users;
            if(!$ID(room.replace(/\s/, '_')+'_group')) addGroupToList(room);
            for(var i=0; i<users.length; i++) {
               if(!$ID(users[i]+'_blItem')) addBuddyToList(users[i], room);
            }
            Windows.close('newRoom');
            IMWindows[room].toFront();
            setTimeout("$ID('"+room+"_sendBox').focus()", 125);
         }
		  });
}

function ping() {
  var xhConn = new XHConn();

  xhConn.connect(pingTo, "POST", "call=ping&from="+user+"&away="+isAway+"&status="+curStatus,
      function(xh) {
        var i;

        if(xh.responseText == 'not_logged_in') {
            //logout();
            return;
        }

        if(trim(xh.responseText).length == 0) return;

        var response = xh.responseText.parseJSON();

        var from, data;
        for(i=0; i<response.numMessages; i++) {
            from = response.messages[i].sender;
            data = response.messages[i].message;
            who  = (response.messages[i].recipient == user ? from : response.messages[i].recipient);

            if(!$ID(who + '_im')) {
              createIMWindow(who, who);
            } else {
              if(!IMWindows[who].detached && !IMWindows[who].isVisible()) {
                  IMWindows[who].show();
                  setTimeout("scrollToBottom('" + who + "_rcvd')", 125);
              }
            }

            Stamp = new Date(); var h = String(Stamp.getHours()); var m = String(Stamp.getMinutes()); var s = String(Stamp.getSeconds());
            h = (h.length > 1) ? h : "0"+h; m = (m.length > 1) ? m : "0"+m;
            var curIM = (!IMWindows[who].detached ? $ID(who+"_rcvd") : IMWindows[who].popup.$ID(who+"_rcvd"));
            data = data.replace(/(\s|\n|>|^)(\w+:\/\/[^<\s\n]+)/, '$1<a href="$2" target="_blank">$2</a>');
            data = emoteReplace(data, smilies);
            if(data.replace(/<([^>]+)>/ig, '').indexOf('/me') == 0)
              curIM.innerHTML = curIM.innerHTML + "<b class=\"userB\">[" + h + ":" + m + "] <i>" + from + ' ' + data.replace(/<([^>]+)>/ig, '').replace(/\/me/, '') + "</i></b><br>\n";
            else
              curIM.innerHTML = curIM.innerHTML + "<b class=\"userB\">[" + h + ":" + m + "] " + from + ":</b> " + data + "<br>\n";
            curIM.scrollTop = curIM.scrollHeight - curIM.clientHeight + 6;

            if(curIM.innerHTML.toLowerCase().replace(/<\S[^>]*>/g, '').indexOf(user.toLowerCase()+': (auto-reply)') == -1 && isAway == 1 && who == from) {
              $ID(from+"_sendBox").value = '(Auto-Reply) ' + awayMessage;
              sendMessage(from);
            }

            if(Windows.getFocusedWindow().getId() != who + "_im" && pulsateTitles == true) {
              new Effect.Pulsate(who + '_im_top');
            }

            if(titlebarBlinker == true && useBlinker == true) {
              clearTimeout(blinkerTimer);
              setTimeout("titlebarBlink('"+who+"', \""+data.replace(/\"/, '\"').replace(/<([^>]+)>/ig, '')+"\", 0)", blinkSpeed);
            }
            curIM = null;
        }
        if(response.numMessages > 0 && audioNotify == true) soundManager.play('msg_in');

        from = null; data = null;
        var group = '';
        var buddy = '';
        var event = '';
        for(i=0; i<response.numEvents; i++) {
            from = response.events[i].sender;
            data = response.events[i].event;
            who  = (response.events[i].recipient == user ? from : response.events[i].recipient);

            event = data.split(',');
            switch(event[0]) {
              case 'status':
                  group = response.events[i].group;
                  if(!$ID(group.replace(/\s/, '_')+'_group') && group != 'toJSONString') addGroupToList(group);
                  if(!$ID(from+'_blItem')) {
                    addBuddyToList(from, group);
                    buddyList[group][buddyList[group].length] = from;
                    $ID(from+'_blItem').style.display='block';
                  }

                  if(event[1] == 0 || event[1] == 50) {
                    moveBuddy(from, 'Offline');
                    $ID(from+'_blImg').src = base_path+'images/offline.png';
                  } else if(event[1] == 2) {
                    moveBuddy(from, group);
                    $ID(from+'_blImg').src = base_path+'images/away.png';
                  } else {
                    moveBuddy(from, group);
                    $ID(from+'_blImg').src = base_path+'images/online.png';
                  }
                  break;
              case 'chat':
                  var rcvdBox = $ID(event[2]+"_rcvd");
                  Stamp = new Date(); var h = String(Stamp.getHours()); var m = String(Stamp.getMinutes()); var s = String(Stamp.getSeconds());
                  h = (h.length > 1) ? h : "0"+h; m = (m.length > 1) ? m : "0"+m;
                  if(event[1] == 'join') {
                    if(!$ID(from+'_blItem')) addBuddyToList(from, event[2]);
                    rcvdBox.innerHTML = rcvdBox.innerHTML + "<b class=\"userB\">[" + h + ":" + m + "] <i>"+from+" has joined.</i></b><br>";
                    scrollToBottom(event[2]+"_rcvd");
                  } else if(event[1] == 'left') {
                    if(typeof(response.events[i]['group']) == 'object') deleteBuddyFromList(from);
                    rcvdBox.innerHTML = rcvdBox.innerHTML + "<b class=\"userB\">[" + h + ":" + m + "] <i>"+from+" has left.</i></b><br>";
                    scrollToBottom(event[2]+"_rcvd");
                  }
                  break;
            }
            event = null;
        }
        from = null; data = null; who = null;
      }
  );
  xhConn = null;
}

function show_room_window(room) {
  if(!$ID(room + '_im')) {
    createIMWindow(room, room);
  } else {
    if(!IMWindows[room].detached && !IMWindows[room].isVisible()) {
        IMWindows[room].show();
        setTimeout("scrollToBottom('" + room + "_rcvd')", 125);
    }
  }
}

function sendMessage(winName) {
  var xhConn = new XHConn();

  var isBold      = ($ID(winName + '_sendBox').style.fontWeight == '400' ? 'false' : 'true');
  var isItalic    = ($ID(winName + '_sendBox').style.fontStyle == 'normal' ? 'false' : 'true');
  var isUnderline = ($ID(winName + '_sendBox').style.textDecoration == 'none' ? 'false' : 'true');
  var fontName    = $ID(winName + '_setFont').innerHTML;
  var fontSize    = $ID(winName + '_setFontSize').innerHTML;
  var fontColor   = $ID(winName + '_setFontColorColor').style.backgroundColor;

  if(trim($ID(winName+"_sendBox").value).length > 0) {
    var sBox = $ID(winName + '_sendBox').value;
    $ID(winName+"_sendBox").value = '';
    xhConn.connect(pingTo, "POST", "call=send&from="+user+"&recipient="+winName+"&bold="+isBold+"&italic="+isItalic+"&underline="+isUnderline+"&font="+fontName+"&fontsize="+fontSize+"&fontcolor="+fontColor+"&msg="+encodeURIComponent(sBox.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/g, "<amp>")),
        function(xh) {
          if(xh.responseText == 'sent') {
              // do nothing!
          } else if(xh.responseText == 'not_online') {
              var rcvdBox = $ID(winName+"_rcvd");
              rcvdBox.innerHTML = rcvdBox.innerHTML + '<span style="color:#FF0000"><b>Error: Your message could not be sent because the recipient is not logged in.</b></span><br>';
              scrollToBottom(winName+"_rcvd");
          } else if(xh.responseText == 'too_long') {
              var rcvdBox = $ID(winName+"_rcvd");
              rcvdBox.innerHTML = rcvdBox.innerHTML + '<span style="color:#FF0000"><b>Error: Your message could not be sent because it is too long.</b></span><br>';
              scrollToBottom(winName+"_rcvd");
          } else if(xh.responseText == 'not_logged_in') {
              //logout();
          } else {
              alert('An error occured while sending your message.');
          }
          $ID(winName+"_sendBox").focus();
        }
    );

    var sentText = sBox;
    sentText = sentText.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/<([^>]+)>/ig, '').replace(/(\s|\n|>|^)(\w+:\/\/[^<\s\n]+)/, '$1<a href="$2" target="_blank">$2</a>');
    sentText = emoteReplace(sentText, smilies);
    var rcvdBox = $ID(winName+"_rcvd");
    Stamp = new Date(); var h = String(Stamp.getHours()); var m = String(Stamp.getMinutes()); var s = String(Stamp.getSeconds());
    h = (h.length > 1) ? h : "0"+h; m = (m.length > 1) ? m : "0"+m;
    if(sentText.replace(/<([^>]+)>/ig, '').indexOf('/me') == 0)
        rcvdBox.innerHTML = rcvdBox.innerHTML + "<b class=\"userA\">[" + h + ":" + m + "] <i>" + user + ' ' + sentText.replace(/<([^>]+)>/ig, '').replace(/\/me/, '') + "</i></b><br>\n";
    else
        rcvdBox.innerHTML = rcvdBox.innerHTML + "<b class=\"userA\">[" + h + ":" + m + "] " + user + ":</b> <span style=\"font-family:" + fontName + ",sans-serif;font-size:" + fontSize + "px;color:" + fontColor + ";\">" + (isBold == 'true' ? "<b>" : "") + (isItalic == 'true' ? "<i>" : "") + (isUnderline == 'true' ? "<u>" : "") + sentText + (isBold == 'true' ? "</b>" : "") + (isItalic == 'true' ? "</i>" : "") + (isUnderline == 'true' ? "</u>" : "") + "</span><br>\n";
    scrollToBottom(winName+"_rcvd");
    if(audioNotify == true) soundManager.play('msg_out');
  }
}

function saveBuddyList() {
   var xhConn = new XHConn();
   //xhConn.connect(pingTo, "POST", "call=save&from="+user+"&list="+encodeURIComponent(buddyList.toJSON()), null);
}

function leave_room_dialog(group) {
   var delGroupWin;
   if($ID('delGroup')) {
      Windows.getWindow('delGroup').toFront();
      return;
   }

   delGroupWin = new Window('delGroup',
                           {className: "dialog", width: 240, height: 70, resizable: false, zIndex: 1,
                            title: "Remove a Group", draggable: true, closable: true, maximizable: false, minimizable: false, detachable: false,
                            minWidth: 240, minHeight:70, showEffectOptions: {duration: 0}, hideEffectOptions: {duration: 0}});
   delGroupWin.getContent().innerHTML = '<div class="dialog_info" style="padding:3px;">Are you sure you want to leave room "<b>' + group + '</b>"?</div> \
                                         <div id="delgroup_buttons"> \
                                         <img src="'+base_path+'images/' + windowCSS + '/ok.png" onclick="leaveRoom(\'' + group + '\');Windows.close(\'delGroup\');"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                         <img src="'+base_path+'images/' + windowCSS + '/cancel.png" onclick="Windows.close(\'delGroup\');"  onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onmousedown="buttonDown(this);" onmouseup="buttonNormal(this);" /> \
                                         </div>';
   $ID('delgroup_buttons').style.position = 'absolute';
   $ID('delgroup_buttons').style.top      = '60px';
   $ID('delgroup_buttons').style.left     = '25px';
   delGroupWin.setDestroyOnClose();
   delGroupWin.showCenter();
}

function addGroupToList(groupname) {
  var bList = $ID('buddylist');

  bList.innerHTML = (groupname=='Offline' ? bList.innerHTML : '') + '<li id="' + groupname.replace(/\s/, '_') + '_groupTop" class="groupTop" onmousedown="return false;" onselectstart="return false;" ondblclick=" toggleGroup(\'' + groupname + '\');"><img id="' + groupname.replace(/\s/, '_') + '_groupArrow" src="'+base_path+'images/' + windowCSS + '/arrow.png" />&nbsp;&nbsp;' + groupname +
                     ((groupname!='Offline' && groupname!='Buddies') ? ' <a href="#" class="delLink" onclick="show_room_window(\'' + groupname + '\');return false;"><img alt="Open chatroom window" src="'+base_path+'images/' + windowCSS + '/smallnewroom.png" style="border:0;" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" /></a> <a href="#" class="delLink" onclick="leave_room_dialog(\'' + groupname + '\');return false;"><img src="'+base_path+'images/' + windowCSS + '/smallx.png" style="border:0;" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" alt="Leave chatroom" /></a>' : '') + '</li>' + "\n" + '<ul id="' + groupname.replace(/\s/, '_') + '_group" class="group"></ul>' + (groupname!='Offline' ? bList.innerHTML : '');
}

function addBuddyToList(username, groupname) {
  if(!$ID(groupname.replace(/\s/, '_') + '_group')) addGroupToList(groupname);

  var groupList = $ID(groupname.replace(/\s/, '_') + '_group');
  groupList.innerHTML += '<li id="'+username+'_blItem" class="buddy" onmousedown="clickBuddy(\''+username+'\');return false;" onselectstart="return false;" onmouseover="selectBuddy(this, \''+username+'\', true);" onmouseout="selectBuddy(this, \''+username+'\', false);" ondblclick="onBuddyDblClick();">&nbsp;&nbsp;&nbsp;&nbsp;<img src="'+base_path+'images/online.png" width="16" height="16" alt="" id="'+username+'_blImg">&nbsp;'+username+'</li>';
   $ID(username + '_blItem').style.listStyleType = 'none';
}

function moveBuddy(username, groupname) {
  if(groupname == null) return;
  if($ID(username+'_blItem').parentNode.id == $ID(groupname.replace(/\s/, '_') + '_group')) return;
  if(!$ID(groupname.replace(/\s/, '_') + '_group')) addGroupToList(groupname);

  $ID(groupname.replace(/\s/, '_') + '_group').insertBefore($ID(username+'_blItem'), null);
}

function selectBuddy(sel, username, selected) {
   if(selected === false) {
      if(curSelected != username) {
         sel.style.background = '#fff';
         sel.style.color = '#333';
      } else {
         sel.style.background = '#d0dae6';
         sel.style.color = '#000';
      }
   } else {
      sel.style.background = '#e1ebf7';
      sel.style.color = '#000';
   }
}

function clickBuddy(username) {
   if(curSelected.length > 0) {
      var sel = $ID(curSelected + '_blItem');
      sel.style.background = '#fff';
      sel.style.color = '#333';
   }

   curSelected = username;

   sel = $ID(curSelected + '_blItem');
   sel.style.background = '#d0dae6';
   sel.style.color = '#333';
}

function onBuddyDblClick() {
   if(curSelected.length > 0) {
      if(!$ID(curSelected + '_im')) {
         createIMWindow(curSelected, curSelected);
      } else {
         if(IMWindows[curSelected].popup) {
            if(IMWindows[curSelected].popup.closed) {
               IMWindows[curSelected] = IMWindows[curSelected].old;
               IMWindows[curSelected].show();
            } else {
               IMWindows[curSelected].popup.focus();
            }
         } else if(!IMWindows[curSelected].isVisible()) {
            IMWindows[curSelected].show();
            IMWindows[curSelected].toFront();
            setTimeout("scrollToBottom('" + curSelected + "_rcvd')", 125);
            setTimeout("$ID('" + curSelected + "_sendBox').focus();", 250);
         } else {
            IMWindows[curSelected].toFront();
            setTimeout("$ID('" + curSelected + "_sendBox').focus();", 250);
         }
      }
   }
}

function scrollToBottom(id) {
   $ID(id).scrollTop = $ID(id).scrollHeight - $ID(id).clientHeight;
}

function deleteBuddyFromList(username) {
   if(username.indexOf('_group') != -1) {
      deleteGroupFromList(username.substring(0, username.length - 6));
      return;
   }

   var usernam = username;

   var ingroup, loc;
   for (var group in buddyList) {
      for(var i=0; i<buddyList[group].length; i++) {
         if(buddyList[group][i] == username) {
            ingroup = group;
            loc = i;
            break;
         }
      }
   }

   var buddyToRmv = $ID(usernam+"_blItem");
   if(typeof(buddyToRmv) !== 'undefined') {
      buddyToRmv.parentNode.removeChild(buddyToRmv);
      if(buddyList[ingroup]) {
         buddyList[ingroup].splice(loc, 1);
         saveBuddyList();
      }
      Dialog.closeInfo();
   }
}

function deleteGroupFromList(groupname) {
   var groupNoSpaces = groupname.replace(/\s/, '_');
   var groupToRmv = $ID(groupNoSpaces+"_group");
   var groupTop   = $ID(groupNoSpaces+"_groupTop");

   if(typeof(groupToRmv) !== 'undefined') {
      groupToRmv.parentNode.removeChild(groupToRmv);
      groupTop.parentNode.removeChild(groupTop);
      delete buddyList[groupname];
      saveBuddyList();
      Dialog.closeInfo();
   } else {
      $ID('deletebuddy_error_msg').innerHTML = 'No such group on buddylist!';
      $ID('deletebuddy_error_msg').show();
      Dialog.win.updateHeight();
   }
}

function toggleGroup(groupname) {
   var groupList = $ID(groupname.replace(/\s/, '_') + '_group');
   var groupArrow = $ID(groupname.replace(/\s/, '_') + '_groupArrow');

   if(groupList.style.display != 'none') {
      groupList.style.display = 'none';
      groupArrow.src = base_path+'images/' + windowCSS + '/arrow_up.png';
   } else {
      groupList.style.display = 'block';
      groupArrow.src = base_path+'images/' + windowCSS + '/arrow.png';
   }
}

function toggleAudio() {
   if(audioNotify == true) {
      audioNotify = false;
      $ID('toggleaudio').src = base_path+'images/'+windowCSS+'/audio_off.png';
   } else {
      audioNotify = true;
      $ID('toggleaudio').src = base_path+'images/'+windowCSS+'/audio_on.png';
   }
}

function toggleBold(name) {
   $ID(name + '_sendBox').style.display = 'none'; // horrah weird Opera 9 input refresh!
   if($ID(name + '_sendBox').style.fontWeight == '400') {
      $ID(name + '_bold').src = base_path+'images/' + windowCSS + '/bold_on.png';
      $ID(name + '_sendBox').style.fontWeight = '700';
   } else {
      $ID(name + '_sendBox').style.fontWeight = '400';
      $ID(name + '_bold').src = base_path+'images/' + windowCSS + '/bold_off.png';
   }
   $ID(name + '_sendBox').style.display = 'block'; // horrah weird Opera 9 input refresh!
   setTimeout("$ID('" + name + "_sendBox').focus();", 125);
}

function toggleItalic(name) {
   $ID(name + '_sendBox').style.display = 'none'; // horrah weird Opera 9 input refresh!
   if($ID(name + '_sendBox').style.fontStyle == 'normal') {
      $ID(name + '_sendBox').style.fontStyle = 'italic';
      $ID(name + '_italic').src = base_path+'images/' + windowCSS + '/italic_on.png';
   } else {
      $ID(name + '_sendBox').style.fontStyle = 'normal';
      $ID(name + '_italic').src = base_path+'images/' + windowCSS + '/italic_off.png';
   }
   $ID(name + '_sendBox').style.display = 'block'; // horrah weird Opera 9 input refresh!
   setTimeout("$ID('" + name + "_sendBox').focus();", 125);
}

function toggleUnderline(name) {
   $ID(name + '_sendBox').style.display = 'none'; // horrah weird Opera 9 input refresh!
   if($ID(name + '_sendBox').style.textDecoration == 'none') {
      $ID(name + '_sendBox').style.textDecoration = 'underline';
      $ID(name + '_underline').src = base_path+'images/' + windowCSS + '/underline_on.png';
   } else {
      $ID(name + '_sendBox').style.textDecoration = 'none';
      $ID(name + '_underline').src = base_path+'images/' + windowCSS + '/underline_off.png';
   }
   $ID(name + '_sendBox').style.display = 'block'; // horrah weird Opera 9 input refresh!
   setTimeout("$ID('" + name + "_sendBox').focus();", 125);
}

function newIMWindow() {
   if($ID('sendto').value.replace(/^\s*|\s*$/g,"").length > 0) {
      var toWhom = $ID('sendto').value;

      if(!$ID(toWhom + '_im')) {
         createIMWindow(toWhom, toWhom);
      } else {
         if(!IMWindows[toWhom].isVisible()) {
            IMWindows[toWhom].show();
            setTimeout("scrollToBottom('" + toWhom + "_rcvd')", 125);
         }
      }

      Windows.close('newIM');
      IMWindows[toWhom].toFront();
      setTimeout("$ID('"+toWhom+"_sendBox').focus()", 125);
   } else {
      $ID('newim_error_msg').innerHTML = 'Please enter a proper username!';
   }
}


function emoteReplace(str, itemsList) {
   var r;
   for(var i=0; i<itemsList.length; i++) {
      r = itemsList[i].split('~');
      if(str.indexOf(r[0]) > -1)
         str = str.replace(new RegExp(regExpEscape(r[0]), 'g'), '<img src="'+base_path+'images/emoticons/' + r[1] + '" alt="' + r[1] + '" title="' + r[0] + '" />');
   }
   return str;
}

function regExpEscape(text) {
  if (!arguments.callee.sRE) {
    var specials = [
      '/', '.', '*', '+', '?', '|',
      '(', ')', '[', ']', '{', '}', '\\'
    ];
    arguments.callee.sRE = new RegExp(
      '(\\' + specials.join('|\\') + ')', 'g'
    );
  }
  return text.replace(arguments.callee.sRE, '\\$1');
}

function createIMWindow(name, imTitle) {
   var imLeft = Math.round(Math.random()*(browserWidth()-360))+'px';
   //var imTop  = Math.round((Math.random()*browserHeight()-400))+'px';
   var imTop = 10;

   IMWindows[name] = new Window(name + '_im', {className: "dialog", width: 320, height: 335, top: imTop, left: imLeft,zIndex: 1,
                                               resizable: true, title: imTitle, draggable: true, detachable: imDetachable, minWidth: 320, minHeight: 150,
                                               showEffectOptions: {duration: 0}, hideEffectOptions: {duration: 0}});
   IMWindows[name].getContent().innerHTML = '<div class="rcvdMessages" id="' + name + '_rcvd"></div>' + "\n" +
                                            '<div class="imToolbar" id="' + name + '_toolbar" onmousemove="return false;" onselectstart="return false;"><img src="'+base_path+'images/'+windowCSS+'/bold_off.png" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onclick="toggleBold(\'' + name + '\');" onmousedown="return false;" alt="Bold" id="' + name + '_bold" /> ' +
                                            '<img src="'+base_path+'images/'+windowCSS+'/italic_off.png" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onclick="toggleItalic(\'' + name + '\');" onmousedown="return false;" alt="Italic" id="' + name + '_italic" /> '+
                                            '<img src="'+base_path+'images/'+windowCSS+'/underline_off.png" onmouseover="buttonHover(this);" onmouseout="buttonNormal(this);" onclick="toggleUnderline(\'' + name + '\');" onmousedown="return false;" alt="Underline" id="' + name + '_underline" /></div>' +
                                            ' <a href="#" class="setFontLink" id="' + name + '_setFont" onclick="toggleFontList(\'' + name + '\');return false;" onselectstart="return false;">Tahoma</a>' +
                                            ' <a href="#" class="setFontSizeLink" id="' + name + '_setFontSize" onclick="toggleFontSizeList(\'' + name + '\');return false;" onselectstart="return false;">12</a>' +
                                            ' <a href="#" class="setFontColorLink" id="' + name + '_setFontColor" onclick="toggleFontColorList(\'' + name + '\');return false;" onselectstart="return false;"><div id="' + name + '_setFontColorColor" style="width:14px;height:14px;display:block;"></div></a>' +
                                            ' <a href="#" class="insertEmoticonLink" id="' + name + '_insertEmoticon" onclick="toggleEmoticonList(\'' + name + '\');return false;" onselectstart="return false;"><img src="'+base_path+'images/emoticons/mini_smile.gif" width="14" height="14" style="border:0;" /></a>' +
                                            "\n" + '<div style="overflow:auto;"><textarea class="inputText" id="'+name+'_sendBox" onfocus="blinkerOn(false);" onkeypress="return keyHandler(event,'+"'"+name+"'"+');"></textarea></div>';
   $ID(name + '_rcvd').style.height = (IMWindows[name].getSize().height - 103) + 'px';
   $ID(name + '_rcvd').style.width = (IMWindows[name].getSize().width - 10) + 'px';

   $ID(name + '_toolbar').style.top = (IMWindows[name].getSize().height - 73) + 'px';
   $ID(name + '_toolbar').style.width = (IMWindows[name].getSize().width - 10) + 'px';

   $ID(name + '_setFont').style.top = (IMWindows[name].getSize().height - 65) + 'px';

   $ID(name + '_setFontSize').style.top = (IMWindows[name].getSize().height - 65) + 'px';

   $ID(name + '_setFontColor').style.top = (IMWindows[name].getSize().height - 65) + 'px';
   $ID(name + '_setFontColorColor').style.backgroundColor = '#000';

   $ID(name + '_insertEmoticon').style.top = (IMWindows[name].getSize().height - 65) + 'px';

   $ID(name + '_sendBox').style.top = (IMWindows[name].getSize().height - 45) + 'px';
   $ID(name + '_sendBox').style.left = '2px';
   $ID(name + '_sendBox').style.width = (IMWindows[name].getSize().width - 16) + 'px';
   $ID(name + '_sendBox').style.fontWeight = '400';
   $ID(name + '_sendBox').style.fontStyle = 'normal';
   $ID(name + '_sendBox').style.textDecoration = 'none';

   IMWindows[name].show();
   IMWindows[name].toFront();
   setTimeout("$ID('"+name+"_sendBox').focus();", 250);
}

function detachIMWindow(name) {
   newWin = name;
   newWinRcvd = $ID(name + '_rcvd').innerHTML;
   IMWindows[name].hide();
   var temp = IMWindows[name];
   var popupWin = window.open(base_path+'popup.html', name + '_im', 'left='+IMWindows[name].getLocation()['left']+',top='+IMWindows[name].getLocation()['top']+',width=320,height=335,toolbar=0,location=1,status=0,menubar=0,resizable=1,scrollbars=0');
   IMWindows[name] = {};
   IMWindows[name].popup = popupWin;
   IMWindows[name].isVisible = function() { return true; };
   IMWindows[name].show = function() { return true; };
   IMWindows[name].detached = true;
   IMWindows[name].old = temp;
}

function buttonHover(el) {
   var newsrc = el.src;
   newsrc = newsrc.replace(/_hover/, '');
   el.src = newsrc.replace(/\.png/, '_hover.png');
}

function buttonDown(el) {
   el.src = el.src.replace(/_hover\.png/, '_down.png');
}

function buttonNormal(el) {
   el.src = el.src.replace(/\_hover.png/, '.png').replace(/\_down.png/, '.png');
}

function browserWidth() {
   if (self.innerWidth) {
      return self.innerWidth;
   } else if (document.documentElement && document.documentElement.clientWidth) {
      return document.documentElement.clientWidth;
   } else if (document.body) {
      return document.body.clientWidth;
   }
   return 630;
}

function browserHeight() {
   if (self.innerWidth) {
      return self.innerHeight;
   } else if (document.documentElement && document.documentElement.clientWidth) {
      return document.documentElement.clientHeight;
   } else if (document.body) {
      return document.body.clientHeight;
   }
   return 470;
}

function randomNumber(max) {
   return Math.round(Math.random()*max);
}

function isDefined(variable) {
   return (typeof(variable) == "undefined") ? false : true;
}

function leaveRoom(room){
  var xhConn = new XHConn();
  xhConn.connect(pingTo, "POST", "call=leaveroom&from="+user+"&room="+room,
    function(xh) {
        if(xh.responseText=='left')
          deleteGroupFromList(room);
          IMWindows[room].destroy();
    });
}

function keyHandler(e, name) {
  var asc = document.all ? event.keyCode : e.which;

  if(asc == 13) {
    sendMessage(name);
    return false;
  }

  return true;
}

function insertText(tti) {
   $ID(toWin + '_sendBox').value += tti;
   setTimeout("$ID('" + toWin + "_sendBox').focus();", 125);
   toggleEmoticonList();
   return false;
}

function setFont(fontname) {
   $ID(toWin + '_sendBox').style.display = 'none';
   $ID(toWin + '_sendBox').style.fontFamily = fontname + ', sans-serif';
   $ID(toWin + '_setFont').innerHTML = fontname;
   $ID(toWin + '_sendBox').style.display = 'block';
   setTimeout("$ID('" + toWin + "_sendBox').focus();", 125);
   toggleFontList('');
}

function setFontSize(size) {
   $ID(toWin + '_sendBox').style.display = 'none';
   $ID(toWin + '_sendBox').style.fontSize = size + 'px';
   $ID(toWin + '_setFontSize').innerHTML = size;
   $ID(toWin + '_sendBox').style.display = 'block';
   setTimeout("$ID('" + toWin + "_sendBox').focus();", 125);
   toggleFontSizeList('');
}

function setFontColor(color) {
   $ID(toWin + '_sendBox').style.color = color;
   $ID(toWin + '_setFontColorColor').style.backgroundColor = color;
   setTimeout("$ID('" + toWin + "_sendBox').focus();", 125);
   toggleFontColorList('');
}

/************************************************/
/* this should be added after all js */