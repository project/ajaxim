Readme
------
Drupal ajaxIM ("asynchronous javascript and xml instant messenger") is a AJAX messenger module. It allows users to communicate with their buddies.

Module development began as integrating ajaxim chat(http://www.unwieldy.net/ajaxim/) into drupal. The result doesn't really look as integrated chat, a lot of code was changed, but a great part of it and idea was taken from original ajaxim.

Features:
1) The messenger buddylist is rendered as a block.
2) List of buddies is automatically fetched from buddylist module.
3) Module supports 6 predefined visibility options and status messages + custom away message
4) Chatroom support
5) Sound notifications

NOTE: THIS MODULE CAN BRAKE YOUR DRUPAL STYLES! - There are css issues currently and module needs testing.

Send comments to Arthur Murauskas at: http://drupal.org/user/66128/contact and Aleksey Khodakovskiy at: http://drupal.org/user/157150/contact

Module was developed while working at Enomaly(http://www.enomaly.com) for BlueShirtNation.com.

Requirements
------------
This module requires Drupal 5.x and buddylist module enabled.

This module does not yet offer PostgreSQL support. If you would like to contribute to this module by creating the appropriate PostgreSQL schema, please submit your code.


Installation
------------
1. Copy the ajaxim directory to the Drupal modules/.

2. Be sure to have buddylist module enabled. Enable ajaxim_server in the "site building | modules" administration screen.

   Enabling the ajaxim_server module will trigger the creation of the database schema. If you are shown error messages you may have to create the schema by hand. Please see the database definition at the end of this file.

3. Enable ajaxim module in the "site building | module" administration screen.

4. Enable ajaxim block in the "blocks" administration screen.

Database Schema
---------------
If the automatic creation of the database tables was unsuccessful you can try creating the tables by hand using the following SQL:

--
-- Table structure for table `ajaxim_chats`
--

CREATE TABLE `ajaxim_chats` (
  `uid` int(11) NOT NULL,
  `room` text NOT NULL,
  `id` bigint(20) NOT NULL
) TYPE=MyISAM /*!40100 DEFAULT CHARACTER SET utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `ajaxim_messages`
--

CREATE TABLE `ajaxim_messages` (
  `recipient` int(11) NOT NULL,
  `room` varchar(255) NOT NULL,
  `sender` int(11) NOT NULL,
  `message` text NOT NULL,
  `type` text NOT NULL,
  `stamp` text NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM /*!40100 DEFAULT CHARACTER SET utf8 */;

-- --------------------------------------------------------

--
-- Table structure for table `ajaxim_status`
--

CREATE TABLE `ajaxim_status` (
  `uid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `last_ping` text NOT NULL,
  `last_status` int(11) NOT NULL,
  `last_stmsg` varchar(50) NOT NULL
) TYPE=MyISAM /*!40100 DEFAULT CHARACTER SET utf8 */;

Credits
-------
Written by Aleksey Khodakovskiy and Arthur Murauskas.
Thanks to Lars-Erik Forsberg for testing.
Thanks to ajaxim authors at http://www.unwieldy.net/ajaxim

Greatest bug to fix:
- Fix problems with css styles

TODO
----
1. Add settings
2. Support for buddy groups
3. Add logs
4. Possible offline messages
5. Provide more clean logic for working on mulpiple pages.